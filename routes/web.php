<?php

use App\Models\User;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return Inertia\Inertia::render('Dashboard');
})->name('dashboard');

Route::middleware(['auth:sanctum', 'verified'])->get('/cabinet', function () {
    return Inertia\Inertia::render('Cabinet');
})->name('Cabinet');

Route::middleware(['auth:sanctum', 'verified'])->get('/votes', function () {
    return Inertia\Inertia::render('Votes');
})->name('Votes');

Route::middleware(['auth:sanctum', 'verified'])->get('/achievements', function () {
    return Inertia\Inertia::render('Achievements');
})->name('Achievements');

Route::middleware(['auth:sanctum', 'verified'])->get('/rules', function () {
    return Inertia\Inertia::render('Rules');
})->name('Rules');

 Route::middleware('auth:sanctum')->get('/users', 'App\Http\Controllers\VoteController@index');
 Route::middleware('auth:sanctum')->post('/votes', 'App\Http\Controllers\VoteController@store');
 Route::middleware('auth:sanctum')->get('/results', 'App\Http\Controllers\VoteController@results');
